import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:thinkpalm_project/ui/config/color/config_color.dart';
import 'package:thinkpalm_project/ui/views/films_list/films_list.dart';
import 'business_logic/view_models/page_view_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PageViewModel>.value(
          value: PageViewModel(),
        ),
      ],
      child: MaterialApp(
        title: 'ThinkPalm',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'titillium-web',
              displayColor: AppCustomColors.textColor),
        ),
        home: FilmList(),
      ),
    );
  }
}
