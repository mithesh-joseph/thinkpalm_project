import 'content.dart';

class PageItem {
  String title;
  String totalContentItems;
  String pageNum;
  String pageSize;
  List<Content> contentItems;

  PageItem();

  PageItem.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json['page'];
    if (data != null) {
      title = data['title'];
      totalContentItems = data['total-content-items'];
      pageNum = data['page-num'];
      pageSize = data['page-size'];

      var items = data['content-items'];
      if (items != null) {
        List content = items['content'];
        if (content != null) {
          if (content != null && content.isNotEmpty)
            contentItems =
                content.map((c) => Content.fromJson(c)).toList();
        }
      }
    }
  }
}
