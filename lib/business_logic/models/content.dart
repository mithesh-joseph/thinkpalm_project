class Content {
  String name;
  String posterImage;

  Content({this.name, this.posterImage});

  Content.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    posterImage = json['poster-image'];
  }
}