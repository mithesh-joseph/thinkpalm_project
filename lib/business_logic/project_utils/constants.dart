library constants;

/// File to set the constant values frequently used in app
///

const String DEFAULT_SEARCH_HINT = 'Search films...';
const String ITEM_UNAVAILABLE_ERROR = 'Sorry Nothing to display..!!!';

const double SCAFFOLD_TOP_PADDING = 36; // Padding from scaffold to AppBar.
const double SCAFFOLD_SIDE_PADDING = 30; // Same padding is applicable to Bottom padding also.
const double GRID_TITLE_TOP_PADDING = 24; // Padding between title and above widget.
const double GRID_MAIN_AXIS_SPACING = 90;
const double GRID_CROSS_AXIS_SPACING = 30;