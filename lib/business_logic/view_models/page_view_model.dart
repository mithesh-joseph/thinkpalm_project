import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thinkpalm_project/business_logic/models/content.dart';
import 'package:thinkpalm_project/business_logic/models/page.dart';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class PageViewModel extends ChangeNotifier {
  static final PageViewModel __sharedInstance = PageViewModel.__internal();

  factory PageViewModel() {
    return __sharedInstance;
  }

  PageViewModel.__internal();

  List<PageItem> _pageItems = List<PageItem>();
  List<Content> _contentItems = List<Content>();
  List<Content> _filteredContents = List<Content>();

  /// Getters to get the different list of items for display.
  ///
  List<PageItem> get getPageItems {
    return _pageItems;
  }

  List<Content> get getContentItems {
    return _contentItems;
  }

  List<Content> get getFilteredContents {
    return _filteredContents;
  }

  /// Method to fetch all Page items one by one as the user scroll down the Gridview.
  /// At first, the Json file 1 will be fetched and parsed to Corresponding models.
  /// Then when user scrolls down, and when it aproaches the end of the first data set,
  /// this method is called again for fetching the second Json file. Like wise, for
  /// the third Json file also. Initially, the pageNum will be zero as it is from
  /// the initialLoading. That is why for the next time, when the pageNum value is
  /// taken from pageItems.last, it incremented by one. Otherwise it will again go
  /// fetching Json-1.
  ///
  Future<void> fetchAllPageItems() async {
    int _currentPage = _pageItems.isEmpty
        ? 1
        : int.parse(_pageItems.last.pageNum)+1;
    var jsonAssetName = '';
    if (_currentPage == 1) {
      jsonAssetName = 'CONTENTLISTINGPAGE-PAGE1.json';
    } else if (_currentPage == 2) {
      jsonAssetName = 'CONTENTLISTINGPAGE-PAGE2.json';
    } else if (_currentPage == 3) {
      jsonAssetName = 'CONTENTLISTINGPAGE-PAGE3.json';
    }

    String data = await rootBundle.loadString('assets/data/$jsonAssetName');
    PageItem page = parseJson(data.toString());

    if (page != null) {
      _pageItems.add(page);
      _contentItems.addAll(page.contentItems);
    }
    notifyListeners();
  }

  /// Method to get the total number of items in a List of Content. It can either
  /// _contentItems or _filteredContents based on the value of 'searchKey'. If
  /// search key contains a value, then it means we need the count of _filteredContents,
  /// as it is the list that contains filetered Content based on the search.
  ///
  int getItemCount(String _searchKey) {
    return _searchKey.isEmpty || _searchKey.length < 3
        ? _contentItems.length
        : _filteredContents.length;
  }

  /// Method to fetch Image from rootBundle when a given path is provided from the
  /// View page. It can return an Image corresponding to the path provided, and also
  /// if there is no such Image found in the given path, it wil throw an error
  /// as a result, the Default image can be returned.
  ///
  Future<Image> getImage(String path) async {
    final assetBasicPath = 'assets/images/posters/';
    return rootBundle.load(assetBasicPath + path).then((value) {
      return Image.asset(
        assetBasicPath + path,
        fit: BoxFit.fill,
      );
    }).catchError((_) {
      return Image.asset(
        assetBasicPath + 'placeholder_for_missing_posters.png',
        fit: BoxFit.fill,
      );
    });
  }

  /// Method to return an intermediate an object of PosterName class, which we
  /// are using for displaying the Poster name under each GridItem when the user
  /// searches for an item and the corresponding match is displayed using a Yellow
  /// and White colored Text. These 2 colors are given using a RichText and the 2 components
  /// with yellow and White color will be the PostName object (PostName.firstPart and
  /// PostName.secondPart).
  ///
  PosterName getPosterName(int index, String searchKey) {
    PosterName posterName = PosterName();
    if (searchKey.isNotEmpty && searchKey.length >= 3) {
      final startIndex = [
        _filteredContents[index].name.substring(0, searchKey.length),
        _filteredContents[index].name.substring(searchKey.length)
      ];
      posterName.firstPart = startIndex[0];
      posterName.secondPart = startIndex[1];
    } else {
      posterName.secondPart = _contentItems[index].name;
    }
    return posterName;
  }

  /// Parse json from the string given. This will parse the string to corresponding
  /// PageItem Model.
  ///
  PageItem parseJson(String response) {
    if (response == null) {
      return null;
    }
    final parsed = json.decode(response.toString());
    return PageItem.fromJson(parsed);
  }

  /// Method that actually implements the Search functionality.
  /// It accepts a searchKey from the View and it filters the content of the Lists
  /// based on the existence of value in the searchKey. That means, if searchKey
  /// contains a value, it will filter the '_contentItems' in such a way that the
  /// list will contain all matches that starts with the searchKey.
  /// If searchKey is empty, then it means we need the actual total _contentItems
  /// list.
  ///
  void buildSearchList(String _searchKey) {
    if (_searchKey.isEmpty || _searchKey.length < 3) {
      _filteredContents = _contentItems;
    } else {
      List<Content> _searchList = List();
      for (int i = 0; i < _contentItems.length; i++) {
        Content content = _contentItems.elementAt(i);
        if ((content.name).toLowerCase().startsWith(_searchKey.toLowerCase())) {
          _searchList.add(content);
        }
      }
      _filteredContents = _searchList;
    }
    notifyListeners();
  }
}

/// Class that simply used for displaying purpose of GridTitle.
/// It has 2 parts firstPart and secondPart, which are used for displaying the
/// text in 2 different colors (Yellow and White) when the user searches and finds
/// the matches.
///
class PosterName {
  String firstPart;
  String secondPart;
}
