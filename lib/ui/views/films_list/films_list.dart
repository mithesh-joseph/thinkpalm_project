import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:thinkpalm_project/business_logic/models/content.dart';
import 'package:thinkpalm_project/business_logic/models/page.dart';
import 'package:thinkpalm_project/business_logic/project_utils/debouncer.dart';
import 'package:thinkpalm_project/business_logic/view_models/page_view_model.dart';
import 'package:provider/provider.dart';
import 'package:flutter/widgets.dart';
import 'package:thinkpalm_project/business_logic/project_utils/constants.dart';
import 'package:thinkpalm_project/ui/config/color/config_color.dart';
import 'package:thinkpalm_project/ui/config/font/config_font_style.dart';

class FilmList extends StatefulWidget {
  @override
  _FilmListState createState() => _FilmListState();
}

class _FilmListState extends State<FilmList>
    with SingleTickerProviderStateMixin {

  /// customSearchArea is used for dispaying the Search Area as well as the title
  /// on AppBar. It is set as a Widget so that it can accept any widget type.
  /// _searchKey is the searchText that we use for searching the Posters given.
  /// customIcon specifies the Image on the different buttons used on the AppBar.
  /// Like, search button image and search cancel button image.
  /// _pageItems contains a list of all PageItems.
  /// _filteredContents contains a list of all filtered Content list. It is used
  /// when searching is used.
  /// _contentItems contains all the Content items. Apart from _filteredContents,
  /// it contains all Content list.
  /// _isLoading prevents a list from fetching when another is in progress.
  /// _deBouncer is used to make a delay in accepting searchKey so that we can
  /// reduce the number of ui rendering while searching.
  /// _devicePixelRatio is used to calculate the actual padding from pixels.

  Widget customSearchArea;
  String _searchKey = '';
  Image customIcon;

  List<PageItem> _pageItems = List<PageItem>();
  List<Content> _filteredContents = List<Content>();
  List<Content> _contentItems = List<Content>();

  bool _isLoading = false;

  static const double EMPTY_SPACE = 10.0;
  final _deBouncer = DeBouncer(milliseconds: 500);
  double _devicePixelRatio;


  @override
  void initState() {
    customIcon = Image.asset('assets/icon/search.png');
    WidgetsBinding.instance.addPostFrameCallback((_) => _getInitialData());
    super.initState();
  }

  /// Called whenever there is a change in the PageViewModel and it notifies
  /// Here we save the updated value to current local variables.
  ///
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final provider = Provider.of<PageViewModel>(context);
    _pageItems = provider.getPageItems;
    _contentItems = provider.getContentItems;
    _filteredContents = provider.getFilteredContents;
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// SafeArea is used to not obscure the content from the notches like device
  /// elements.
  ///
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      left: true,
      right: true,
      child: Scaffold(
        appBar: _buildAppBar(),
        body: _bodyWidget(),
      ),
    );
  }

  /// Here we can see some padding calculations, conversion from Pixels to
  /// actual padding.
  /// Orentation builder is used for handling the device rotation and we need to
  /// show the GridCount as 3 in portrait and 5 as in landscape mode.
  ///
  Widget _bodyWidget() {
    final itemCount = PageViewModel().getItemCount(_searchKey);

    _devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    final topPadding = SCAFFOLD_TOP_PADDING / _devicePixelRatio;
    final sidePadding = SCAFFOLD_SIDE_PADDING / _devicePixelRatio;

    return OrientationBuilder(
      builder: (context, orientation) {
        return itemCount == 0
            ? _errorWidget()
            : Container(
                padding: EdgeInsets.only(
                    left: sidePadding,
                    top: topPadding,
                    right: sidePadding,
                    bottom: topPadding),
                color: Colors.black,
                child: _gridView(itemCount, orientation),
              );
      },
    );
  }

  /// Error widget is used to display the message when nothing is there to show
  /// on the screen.
  ///
  Widget _errorWidget() {
    return Container(
      color: Colors.black,
      child: Center(
        child: Text(ITEM_UNAVAILABLE_ERROR, style: AppCustomStyle.normalStyle),
      ),
    );
  }

  /// Main GridView widget to display all Content values on the screen.
  /// Here we can see the setting of crossAxisCount based on the device orientation.
  /// We follow 16/9 childAspectRatio for the Grid.
  /// Here when the user scrolls the grid, and when he reaches the end of current
  /// DataSet, ie, when he reaches at (currentDataSet total count - 6th element),
  /// we fetches the second Dataset from the Json file. Likewise for the third Json also.
  /// If there is any delay in displaying the Corresponding item, a
  /// CircularProgressIndicator will be shown. Then the Image and the Title is shown.
  ///
  Widget _gridView(int itemCount, Orientation orientation) {
    final mainAxisSpacing = GRID_MAIN_AXIS_SPACING / _devicePixelRatio;
    final crossAxisSpacing = GRID_CROSS_AXIS_SPACING / _devicePixelRatio;

    return GridView.builder(
      itemCount: itemCount,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: orientation == Orientation.portrait ? 3 : 5,
          mainAxisSpacing: mainAxisSpacing,
          crossAxisSpacing: crossAxisSpacing,
          childAspectRatio: 9 / 16),
      itemBuilder: (BuildContext context, int index) {
        final count = PageViewModel().getItemCount(_searchKey);
        if (index == count - 6 && _pageItems.length != 3) {
          // Don't trigger _getUpdatedData() if one async loading is already running.
          if (!_isLoading) {
            _getUpdatedData(context);
          }
          return _loadingIndicator();
        }

        return _getGridTile(index);
      },
    );
  }

  /// For showing the Loading
  ///
  Widget _loadingIndicator() {
    return Center(
      child: SizedBox(
        child: CircularProgressIndicator(),
        height: 24,
        width: 24,
      ),
    );
  }

  /// This tile contains the GridTile, which displays the Title as well as the
  /// Image of the Poster on the screen.
  ///
  Widget _getGridTile(int index) {
    return GridTile(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _posterImage(index),
          _posterTitle(index),
        ],
      ),
    );
  }

  /// Widget to display the Image of the Poster on the screen.
  /// Image data to be used will be based on the searchKey because, if there is a valid
  /// searchKey, then it means we need to take the imageData from the _filteredContents.
  /// Otherwise frm the _contentItems.
  ///
  Widget _posterImage(int index) {
    return Expanded(
        child: _getPoster(_searchKey.isEmpty || _searchKey.length < 3
            ? _contentItems[index].posterImage
            : _filteredContents[index].posterImage));
  }

  /// Widget to display the title of each Content items.
  /// Here we seek the help of the small class PosterName for displaying the
  /// text in different colors.
  ///
  Widget _posterTitle(int index) {
    PosterName posterName = PageViewModel().getPosterName(index, _searchKey);
    final topPadding = GRID_TITLE_TOP_PADDING / _devicePixelRatio;

    return Padding(
      padding: EdgeInsets.only(
        top: topPadding,
      ),
      child: RichText(
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        text: TextSpan(
          children: [
            TextSpan(
              text: posterName.firstPart != null ? posterName.firstPart : '',
              style: AppCustomStyle.yellowColorStyle.copyWith(fontSize: 16),
            ),
            TextSpan(
              text: posterName.secondPart,
              style: AppCustomStyle.normalStyle.copyWith(fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }

  /// Widget to display the Image on the screen. If a valid image is found at the
  /// specified path, it will display the corresponding image and if nothing is found
  ///  on the path, then it will show the default image in the Image area.
  ///
  Widget _getPoster(String assetName) {
    return FutureBuilder(
      future: _getImage(assetName),
      builder: (BuildContext context, AsyncSnapshot<Image> snapshot) {
        if (snapshot.data != null) return snapshot.data;
        return Container();
      },
    );
  }

  Future<Image> _getImage(String assetName) async {
    return await PageViewModel().getImage(assetName);
  }

  /// Configuring the Appbar. It contains some custom widgets as we require to
  /// display 2 different set of widgets, one for search feature and the other for
  /// normal title display. When user taps on the search button, appbar turns into
  /// a search area. When user closes the search, it again come back to the normal
  /// title display area.
  ///
  Widget _buildAppBar() {
    return PreferredSize(
      child: Container(
        decoration: _appBarDecoration(),
        child: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/icon/nav_bar.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          elevation: 15.0,
          title: _getAppBarTitle(),
          centerTitle: false,
          leading: _appBarLeadingBtn(),
          backgroundColor: Colors.black,
          actions: <Widget>[
            _customAppBarActionWidget(),
          ],
        ),
      ),
      preferredSize: Size.fromHeight(kToolbarHeight),
    );
  }

  BoxDecoration _appBarDecoration() {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Colors.black,
          offset: Offset(0, 30.0),
          blurRadius: 25.0,
        )
      ],
    );
  }

  /// If customSearchArea is not null, we need that widget, otherwise we need the
  /// Title widget on the AppBAr.
  ///
  Widget _getAppBarTitle() {
    return customSearchArea != null
        ? customSearchArea
        : _getDefaultSearchTitle();
  }

  /// The back button displayed on the AppBar.
  ///
  Widget _appBarLeadingBtn() {
    return IconButton(
      icon: SizedBox(
        height: 20,
        width: 20,
        child: Image.asset('assets/icon/back.png'),
      ),
      onPressed: () {},
    );
  }

  /// The title to be displayed. Since all the PageItems have the same title,
  /// we are using the title of the first PageItem. Hence we took the
  /// _pageItems[0].title
  ///
  Widget _getDefaultSearchTitle() {
    return Text(
      _pageItems.isNotEmpty && _pageItems[0] != null ? _pageItems[0].title : '',
      style: AppCustomStyle.normalStyle.copyWith(
        fontSize: 21.0,
      ),
      textScaleFactor: 1.0,
    );
  }

  /// Search button and search cancel button are configured in this widget.
  /// This is basically the action widget of AppBar.
  /// It handles the _appBarActionBtnTap and performs the required operations
  /// depends on the chosen options.
  ///
  Widget _customAppBarActionWidget() {
    return IconButton(
      icon: SizedBox(
        height: 20,
        width: 20,
        child: customIcon,
      ),
      onPressed: () {
        _appBarActionBtnTap();
      },
    );
  }

  void _appBarActionBtnTap() {
    setState(
      () {
        _updateAppBarItems();
      },
    );
  }

  /// We need to change the action buttons based on what users are doing.
  /// Here we are doing that. We change the search image to search cancel image when
  /// user taps on search and vice versa.
  /// Also changing the Title and SearchArea widget accordingly.
  ///
  void _updateAppBarItems() {
    final iconImage = Image.asset('assets/icon/search.png');

    if (this.customIcon.image == iconImage.image) {
      this.customIcon = Image.asset('assets/icon/search_cancel.png');
      this.customSearchArea = _getSearchTextField();
    } else {
      this.customIcon = iconImage;
      this.customSearchArea = _getDefaultSearchTitle();
      _searchKey = "";
    }
  }

  /// TextField for searching the Posters.
  /// _deBouncer is added to increase the time so that when the user taps say,
  /// 3 charactes, then only it will count the value. Means we dont need to rebuildon each tap,
  /// instead letting the user type and inbetween we are catching the typed value
  /// and we are doiing the operations.
  ///
  Widget _getSearchTextField() {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      autofocus: true,
      decoration: InputDecoration(
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: AppCustomColors.textColor),
        ),
        hintStyle: AppCustomStyle.normalStyle,
        hintText: DEFAULT_SEARCH_HINT,
      ),
      style: AppCustomStyle.normalStyle,
      textInputAction: TextInputAction.go,
      onChanged: (string) {
        _deBouncer.run(
          () {
            _searchKey = string.toLowerCase();
            Provider.of<PageViewModel>(context, listen: true)
                .buildSearchList(_searchKey);
          },
        );
      },
    );
  }

  /// This is called for the first time to get the data of Fisrt Json.
  ///
  _getInitialData() {
    if (!_isLoading) {
      _getUpdatedData(context);
    }
  }

  /// This is called whenever we need to fetch the new values from the Json.
  ///
  void _getUpdatedData(BuildContext context) {
    Provider.of<PageViewModel>(context, listen: true).fetchAllPageItems();
  }
}
