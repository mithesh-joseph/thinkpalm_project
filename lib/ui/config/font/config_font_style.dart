/// Class to set the common FontStyle values that we frequently use in app
///
///
import 'package:flutter/material.dart';
import 'package:thinkpalm_project/ui/config/color/config_color.dart';

class AppCustomStyle {
  static const TextStyle normalStyle = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.normal,
      fontFamily: 'titillium-web',
      color: AppCustomColors.textColor);

  static const TextStyle yellowColorStyle = TextStyle(
      fontSize: 16.0,
      fontWeight: FontWeight.normal,
      fontFamily: 'titillium-web',
      color: Colors.yellow);
}
