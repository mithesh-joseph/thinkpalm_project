/// Class to set the common color values that we frequently use in app
///
 import 'package:flutter/material.dart';

class AppCustomColors
{
  static const Color textColor = Colors.white;
}